package com.example.manggar_laptop.fingerauth.utils;

import android.app.Activity;
import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.example.manggar_laptop.fingerauth.VolleyCallback;

public class Error {
  Activity activity;
  Context context;

  public Error(Activity activity) {
    this.activity = activity;
  }

  public Error(Context ctx) {
    this.context = ctx;
  }

  public void checkOnErrorVolleyNetwork(VolleyError error){
    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
      //This indicates that the reuest has either time out or there is no connection
      new Message(this.activity).showMessage("Tidak ada koneksi atau koneksi Timeout ");

    } else if (error instanceof AuthFailureError) {
      //Error indicating that there was an Authentication Failure while performing the request
      new Message(this.activity).showMessage("Request koneksi failed");

    } else if (error instanceof ServerError) {
      //Indicates that the server responded with a error response
      new Message(this.activity).showMessage("Halaman Tidak Ditemukan");

    } else if (error instanceof NetworkError) {
      //Indicates that there was network error while performing the request
      new Message(this.activity).showMessage("Failed Request pada saat Koneksi");

    } else if (error instanceof ParseError) {
      // Indicates that the server response could not be parsed
      new Message(this.activity).showMessage("Failed Request tidak ada respon");

    }else{
      new Message(this.activity).showMessage("Failed");
    }
  }

  public void checkOnErrorVolleyNetwork(VolleyError error, VolleyCallback callback){
    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
      //This indicates that the reuest has either time out or there is no connection
      new Message(this.activity).showMessage("Tidak ada koneksi atau koneksi Timeout ");
//      new Message(this.activity).showMessage(error.getMessage().toLowerCase());

    } else if (error instanceof AuthFailureError) {
      //Error indicating that there was an Authentication Failure while performing the request
      new Message(this.activity).showMessage("Request koneksi failed");

    } else if (error instanceof ServerError) {
      //Indicates that the server responded with a error response
      new Message(this.activity).showMessage("Halaman Tidak Ditemukan");

    } else if (error instanceof NetworkError) {
      //Indicates that there was network error while performing the request
      new Message(this.activity).showMessage("Failed Request pada saat Koneksi");

    } else if (error instanceof ParseError) {
      // Indicates that the server response could not be parsed
      new Message(this.activity).showMessage("Failed Request tidak ada respon");

    }else{
      new Message(this.activity).showMessage("Failed");
    }

    callback.onError();
  }


  public void checkOnErrorVolleyNetworkContext(VolleyError error, VolleyCallback callback){
    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
      //This indicates that the reuest has either time out or there is no connection
      new Message(this.context).showMessage("Tidak ada koneksi atau koneksi Timeout ");
//      new Message(this.activity).showMessage(error.getMessage().toLowerCase());

    } else if (error instanceof AuthFailureError) {
      //Error indicating that there was an Authentication Failure while performing the request
      new Message(this.context).showMessage("Request koneksi failed");

    } else if (error instanceof ServerError) {
      //Indicates that the server responded with a error response
      new Message(this.context).showMessage("Halaman Tidak Ditemukan");

    } else if (error instanceof NetworkError) {
      //Indicates that there was network error while performing the request
      new Message(this.context).showMessage("Failed Request pada saat Koneksi");

    } else if (error instanceof ParseError) {
      // Indicates that the server response could not be parsed
      new Message(this.context).showMessage("Failed Request tidak ada respon");

    }else{
      new Message(this.context).showMessage("Failed");
    }

    callback.onError();
  }


}
