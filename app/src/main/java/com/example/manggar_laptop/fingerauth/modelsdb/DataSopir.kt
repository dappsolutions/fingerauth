package com.example.manggar_laptop.fingerauth.modelsdb

import com.google.gson.annotations.SerializedName

data class DataSopir(
    @SerializedName("id")
    var id:String,
    @SerializedName("fullname")
    var fullname:String
){
    object MODULE{
        const val FINGER_MOBILE = "finger_mobile"
    }
}