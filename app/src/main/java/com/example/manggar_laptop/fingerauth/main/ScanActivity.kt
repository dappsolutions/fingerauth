package com.example.manggar_laptop.fingerauth.main

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.SparseArray
import com.example.manggar_laptop.fingerauth.R
import com.google.android.gms.vision.barcode.Barcode
import info.androidhive.barcode.BarcodeReader
import android.content.Intent
import android.util.Log
import com.example.manggar_laptop.fingerauth.utils.Message


class ScanActivity : AppCompatActivity(), BarcodeReader.BarcodeReaderListener {

    lateinit var barcodeReader: BarcodeReader
    var id_bahan_baku = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.scan_barcode_view)

        barcodeReader = supportFragmentManager.findFragmentById(R.id.barcode_scanner) as BarcodeReader
        id_bahan_baku = intent.getStringExtra("id_bahan_baku")
    }

    override fun onBitmapScanned(sparseArray: SparseArray<Barcode>?) {

    }

    override fun onScannedMultiple(barcodes: MutableList<Barcode>?) {

    }

    override fun onCameraPermissionDenied() {

    }

    override fun onScanned(barcode: Barcode?) {
        // playing barcode reader beep sound
        barcodeReader.playBeep()

        Log.e("Barcode", barcode?.displayValue)
        // ticket details activity by passing barcode
        val barcodeAct = Intent(this@ScanActivity, MainActivity::class.java)
        barcodeAct.putExtra("qrcode", barcode?.displayValue)
        barcodeAct.putExtra("id_bahan_baku", id_bahan_baku)
        startActivity(barcodeAct)
    }

    override fun onScanError(errorMessage: String?) {
        Message(this).showMessage("Gagal Scan")
    }
}
