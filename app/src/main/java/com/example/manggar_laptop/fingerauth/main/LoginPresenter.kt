package com.example.manggar_laptop.fingerauth.main

import android.support.v4.app.FragmentActivity
import android.util.Log
import com.example.manggar_laptop.fingerauth.VolleyCallback
import com.example.manggar_laptop.fingerauth.utils.ApiUrl
import com.example.manggar_laptop.fingerauth.utils.Message
import com.example.manggar_laptop.fingerauth.utils.Network
import kotlinx.android.synthetic.main.form_login.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import org.json.JSONObject

class LoginPresenter(val main: FragmentActivity) {
    init {

    }


    fun execLogin(){
        Log.e("Login", "OK")

        val url = ApiUrl.API.baseUrl("finger_mobile", "loginUser")
        val params = HashMap<String, String>()
        params.put("username", main.txtUser.text.toString())
        params.put("password", main.txtPassword.text.toString())

        val msg = Message(main)
        msg.showLoading("Proses Login...")
        doAsync {
            Network(main).volleyRequest(url, params, object : VolleyCallback{
                override fun onSuccess(result: String) {
                    msg.hideLoading()
                    if(JSONObject(result).get("is_valid").toString().equals("true")){
                        main.finish()
                    }else{
                        main.toast(JSONObject(result).get("message").toString())
                    }
                }
                override fun onError() {
                    msg.hideLoading()
                    msg.showDialog("Pesan", "Gagal Login")
                }
            })
        }
    }
}