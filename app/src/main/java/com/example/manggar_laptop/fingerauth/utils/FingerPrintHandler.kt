package com.example.manggar_laptop.fingerauth.utils

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.hardware.fingerprint.FingerprintManager
import android.os.Build
import android.os.CancellationSignal
import android.support.annotation.RequiresApi
import android.support.v4.app.ActivityCompat
import android.util.Base64
import org.jetbrains.anko.toast

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.manggar_laptop.fingerauth.R
import com.example.manggar_laptop.fingerauth.main.DetailSopirActivity
import com.example.manggar_laptop.fingerauth.main.MainPresenter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.jetbrains.anko.alert
import org.jetbrains.anko.okButton
import org.jetbrains.anko.startActivity
import javax.crypto.Cipher


@RequiresApi(Build.VERSION_CODES.M)
class FingerPrintHandler(
    var ctx: Context,
    val mainPresenter: MainPresenter,
    val cipher: Cipher
) : FingerprintManager.AuthenticationCallback() {
    lateinit var cancellationSignal: CancellationSignal
    fun startAuth(
        manager: FingerprintManager,
        criptoObject: FingerprintManager.CryptoObject
    ) {
        cancellationSignal = CancellationSignal()
        if (ActivityCompat.checkSelfPermission(
                ctx,
                Manifest.permission.USE_FINGERPRINT
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        manager.authenticate(criptoObject, cancellationSignal, 0, this, null)
    }

    override fun onAuthenticationError(errorCode: Int, errString: CharSequence?) {
        super.onAuthenticationError(errorCode, errString)
//        stopFingerPrint()
//        ctx.toast("Error " + errString)
    }

    override fun onAuthenticationHelp(helpCode: Int, helpString: CharSequence?) {
        super.onAuthenticationHelp(helpCode, helpString)
        ctx.alert {
            title = "Pesan"
            message = helpString.toString()
            ctx.setTheme(R.style.MyDialogTheme)

            okButton {
                mainPresenter.initFingerPrint()
                mainPresenter.getDataKavling()
            }
        }.show()
    }

    override fun onAuthenticationFailed() {
        super.onAuthenticationFailed()
//        ctx.toast(Base64.encodeToString(cipher.iv, Base64.URL_SAFE))
//        ctx.toast(cryptoObject.toString())
//        ctx.startActivity<DetailSopirActivity>("key" to Base64.encodeToString(cipher.iv, Base64.URL_SAFE))
        ctx.alert {
            title = "Pesan"
            message = "Sidik Jari Belum Terdaftar"
            ctx.setTheme(R.style.MyDialogTheme)

            okButton {
                mainPresenter.initFingerPrint()
                mainPresenter.getDataKavling()
            }
        }.show()
    }

    override fun onAuthenticationSucceeded(result: FingerprintManager.AuthenticationResult?) {
        super.onAuthenticationSucceeded(result)

//        ctx.startActivity<DetailSopirActivity>("key" to Base64.encodeToString(cipher.iv, Base64.URL_SAFE))
        GlobalScope.launch(Dispatchers.Main) {
            val stringRequest =
                object : StringRequest(Request.Method.POST,
                    ApiUrl.API.baseUrl("finger_mobile", "simpanFinger"),
                    object : Response.Listener<String> {
                        override fun onResponse(response: String) {
                            ctx.toast("Sidik Jari Valid")
//                            ctx.startActivity<DetailSopirActivity>()
                            ctx.alert {
                                title = "Kavling"
                                message = response
                                ctx.setTheme(R.style.MyDialogTheme)
                                okButton {
                                    mainPresenter.initFingerPrint()
                                    mainPresenter.getDataKavling()
                                }
                            }.show()
                        }
                    },
                    object : Response.ErrorListener {
                        override fun onErrorResponse(error: VolleyError) {
                            ctx.alert {
                                title = "Pesan"
                                message = "Jaringan Bermasalah"
                                ctx.setTheme(R.style.MyDialogTheme)
                                okButton {
                                    mainPresenter.initFingerPrint()
                                    mainPresenter.getDataKavling()
                                }
                            }.show()
                        }
                    }) {
                    override fun getParams(): Map<String, String> {
                        val params = HashMap<String, String>()
                        params["key"] = Base64.encodeToString(cipher.iv, Base64.URL_SAFE)
                        return params
                    }
                }

            Volley.newRequestQueue(ctx).add(stringRequest)
        }
    }

    fun stopFingerPrint() {
        if (cancellationSignal != null && !cancellationSignal.isCanceled()) {
            cancellationSignal.cancel()
        }
    }
}