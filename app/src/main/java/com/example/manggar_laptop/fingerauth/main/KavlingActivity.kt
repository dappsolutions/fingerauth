package com.example.manggar_laptop.fingerauth.main

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View.VISIBLE
import com.example.manggar_laptop.fingerauth.R
import kotlinx.android.synthetic.main.main_view.*
import org.jetbrains.anko.startActivity

class KavlingActivity : AppCompatActivity() {
    lateinit var presenter : KavlingPresenter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_view)
        presenter = KavlingPresenter(this)

//        btnScan.visibility = VISIBLE
        btnScan.setOnClickListener {
            startActivity<ScanActivity>("id_bahan_baku" to presenter.id_bahan_baku)
        }

        btnRefresh.setOnClickListener {
            presenter.getDataKavling()
        }
    }


}
