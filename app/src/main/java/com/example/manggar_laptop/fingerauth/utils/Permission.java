package com.example.manggar_laptop.fingerauth.utils;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;

public class Permission {
  Activity activity;

  int PERMISSION_REQUEST_CODE = 1;


  public Permission(Activity act) {
    this.activity = act;
  }

  public Boolean hasAccessCamera(){
    if (ActivityCompat.checkSelfPermission(this.activity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
      ActivityCompat.requestPermissions(this.activity, new String[]{"android.permission.CAMERA"}, PERMISSION_REQUEST_CODE);
      return true;
    }
    return false;
  }

  public Boolean hasAccessInternet(){
    if (ActivityCompat.checkSelfPermission(this.activity, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) {
      ActivityCompat.requestPermissions(this.activity, new String[]{"android.permission.INTERNET"}, PERMISSION_REQUEST_CODE);
      return true;
    }
    return false;
  }
}
