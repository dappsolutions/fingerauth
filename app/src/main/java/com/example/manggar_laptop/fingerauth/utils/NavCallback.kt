package com.example.manggar_laptop.fingerauth
import android.view.MenuItem


interface NavCallback{
    fun getItemData(item: MenuItem)
}