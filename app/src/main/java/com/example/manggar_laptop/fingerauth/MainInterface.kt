package com.example.manggar_laptop.fingerauth

import android.support.v4.app.FragmentActivity

interface MainInterface{
    fun showMessage(message:String)
    fun showDialog()
    fun hideDialog()
    fun getActivity():FragmentActivity
    fun reloadActivity()
}