package com.example.manggar_laptop.fingerauth.main

import android.graphics.Color
import android.util.Log
import android.view.View.GONE
import android.view.View.VISIBLE
import com.example.manggar_laptop.fingerauth.VolleyCallback
import com.example.manggar_laptop.fingerauth.utils.ApiUrl
import com.example.manggar_laptop.fingerauth.utils.Network
import com.google.gson.Gson
import kotlinx.android.synthetic.main.main_view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.toast

class KavlingPresenter(val main:KavlingActivity){
    var id_bahan_baku:String
    init {
        try{
            id_bahan_baku = main.intent.getStringExtra("id_bahan_baku")
            Log.e("ID BAHAN BAKU", id_bahan_baku)
        }catch(ex:Exception){
            id_bahan_baku = ""
        }

        Log.e("id bahan baku", id_bahan_baku)
        getDataKavling()
    }

    fun getDataKavling() {
        val url = ApiUrl.API.baseUrl("finger_mobile", "getPerintahTimbangActive")
        main.loading.visibility = VISIBLE
        main.cardViewCenter.visibility = GONE
        GlobalScope.launch (Dispatchers.Main) {
            try{
                var params = HashMap<String, String>()
                params["unit"] = "1"
                params["id_bb"] = id_bahan_baku
                Network(main.applicationContext).volleyRequestContext(url, params,object : VolleyCallback{
                    override fun onSuccess(result: String) {
                        main.loading.visibility = GONE
                        main.cardViewCenter.visibility = VISIBLE

                       try{
                           val data = Gson().fromJson(result, KavlingModelResponse::class.java)
                           if(!data.data.isEmpty()){
                               main.contentLayout.setBackgroundColor(Color.parseColor("#FFFFFF"))
                               main.btnScan.visibility = VISIBLE
                               setDataKavling(data.data)
                           }else{
                               main.contentLayout.setBackgroundColor(Color.parseColor("#727272"))
                               main.kavling.text = "Tidak Ada"
                               main.btnScan.visibility = GONE
                               main.btnScan.visibility = VISIBLE
                           }
                       }catch (ex:Exception){
                           Log.e("Error", ex.message.toString())
                       }
                    }

                    override fun onError() {
                        main.toast("Jaringan Lemah")
                    }
                })
            }catch (ex:Exception){
                Log.e("Error", ex.message.toString())
            }
        }
    }

    fun setDataKavling(data: List<KavlingModel>) {
        main.kavling.text = data[0].kavling
    }


}