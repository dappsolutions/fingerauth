package com.example.manggar_laptop.fingerauth

interface VolleyCallback {
  fun onSuccess(result:String)
  fun onError()
}