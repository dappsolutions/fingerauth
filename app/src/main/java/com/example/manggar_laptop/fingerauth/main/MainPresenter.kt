package com.example.manggar_laptop.fingerauth.main

import android.Manifest
import android.app.KeyguardManager
import android.content.Context.FINGERPRINT_SERVICE
import android.content.Context.KEYGUARD_SERVICE
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.hardware.fingerprint.FingerprintManager
import android.os.Build
import android.os.CountDownTimer
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import android.support.v4.app.ActivityCompat
import android.support.v4.app.FragmentActivity
import android.util.Log
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.TextView
import java.security.KeyStore
import javax.crypto.Cipher
import javax.crypto.KeyGenerator
import javax.crypto.SecretKey
import com.example.manggar_laptop.fingerauth.*
import com.example.manggar_laptop.fingerauth.modelsdb.DataKavling
import com.example.manggar_laptop.fingerauth.modelsdb.DataKavlingResponses
import com.example.manggar_laptop.fingerauth.modelsdb.DataPkbb
import com.example.manggar_laptop.fingerauth.modelsdb.DataPkbbResponses
import com.example.manggar_laptop.fingerauth.utils.*
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.jetbrains.anko.design.snackbar
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import org.json.JSONObject
import kotlin.Exception

class MainPresenter(var main: MainInterface) {

    lateinit var keyStore: KeyStore
    var KEY_NAME: String = "sopir"
    lateinit var cipher: Cipher
    lateinit var key:SecretKey
    var mains: FragmentActivity
    lateinit var adapter:RvAdapter
    var dataPkbb:ArrayList<DataPkbb>

    lateinit var timer: CountDownTimer
    var qrcode:String = ""
    var id_bahan_baku:String = ""
    init {
        this.mains = main.getActivity()
        dataPkbb = ArrayList()

        if(main.getActivity().intent.extras != null){
            qrcode = main.getActivity().intent.getStringExtra("qrcode")
            id_bahan_baku = main.getActivity().intent.getStringExtra("id_bahan_baku")
        }
    }

    fun initFingerPrint() {
        val keyguardManager: KeyguardManager = mains.getSystemService(KEYGUARD_SERVICE) as KeyguardManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            try{
                var fingerprintManager: FingerprintManager =
                    mains.getSystemService(FINGERPRINT_SERVICE) as FingerprintManager

                if (!fingerprintManager.isHardwareDetected) {
                    main.showMessage("Smartphone Anda Tidak Support Finger Print")
                } else {
                    if (ActivityCompat.checkSelfPermission(mains, Manifest.permission.USE_FINGERPRINT) !=
                        PackageManager.PERMISSION_GRANTED
                    ) {
                        main.showMessage("Permission Finger Print Belum Aktif")
                    } else {
                        if(fingerprintManager.hasEnrolledFingerprints()){
                            generateKey()
                            if (cipherInit()) {
                                var cryptoObject: FingerprintManager.CryptoObject = FingerprintManager.CryptoObject(cipher)
                                var helper: FingerPrintHandler = FingerPrintHandler(mains, this, cipher)
//                            GlobalScope.launch {
                                helper.startAuth(fingerprintManager, cryptoObject)
//                            }
                            } else {
                                main.showMessage("Salah")
                            }
                        }else{
                            mains.toast("Finger Print Harus Didaftarkan")
                        }
                    }
                }
            }catch (ex:Exception){
                main.showMessage("Tidak Ada Finger print Ditemukan ${ex.message}")
            }
        } else {
            main.showMessage("OS Tidak Support")
        }
    }

    private fun cipherInit(): Boolean {
        try {
            cipher = Cipher.getInstance(KeyProperties.KEY_ALGORITHM_AES + "/" + KeyProperties.BLOCK_MODE_CBC + "/" + KeyProperties.ENCRYPTION_PADDING_PKCS7)
            keyStore.load(null)
            key = keyStore.getKey(KEY_NAME, null) as SecretKey
            cipher.init(Cipher.ENCRYPT_MODE, key)
            return true
        } catch (ex: Exception) {
            return false
        }
    }

    private fun generateKey() {
        keyStore = KeyStore.getInstance("AndroidKeyStore")

        var keyGenerator: KeyGenerator
        keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore")

        keyStore.load(null)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            keyGenerator.init(
                KeyGenParameterSpec.Builder(
                    KEY_NAME, KeyProperties.PURPOSE_ENCRYPT
                )
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    .build()
            )
        }
        keyGenerator.generateKey()
    }

    fun getDataKavling() {
        main.showDialog()
        GlobalScope.launch(Dispatchers.Main) {
            val url = ApiUrl.API.baseUrl(DataKavling.MODULE.FINGER_MOBILE, "dataKavlingBaru")
            val params = HashMap<String, String>()
            params["kavling"] = qrcode
            params["unit"] = "1"
            params["id_bb"] = id_bahan_baku
            Network(mains).volleyRequest(url, params, object : VolleyCallback{
                override fun onSuccess(result: String) {
                    Log.e("DataKAvling", result)
                    try{
                        if(JSONObject(result).get("status").toString().equals("1")){
                            var data = Gson().fromJson(result, DataKavlingResponses::class.java)
                            setDetailData(data.data)
                            var data_kavling = Gson().fromJson(result, DataPkbbResponses::class.java)
                            getDataPkbb(data_kavling.kavling_pkbb)

                            main.getActivity().toast(JSONObject(result).get("message_trans").toString())
                        }else{
                            main.getActivity().toast(JSONObject(result).get("message").toString())
                            var kavling = Intent(main.getActivity(), KavlingActivity::class.java)
                            kavling.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            main.getActivity().startActivity(kavling)
                        }
                    }catch (ex:Exception){
                        Log.e("Error", ex.message.toString())
                    }
                    main.hideDialog()
                }

                override fun onError() {
                    mains.contentLayout.snackbar("Jaringan Bermasalah")
                    main.hideDialog()
                }

            })
        }
    }

    private fun setDetailData(data: List<DataKavling>) {
        if(data.size > 1){
            mains.kavling.text = data[0].kavling+" dan "+data[1].kavling
            mains.bahan_baku.text = data[0].bahan_baku+" dan "+data[1].bahan_baku
            mains.vendor.text = data[0].supplier+" dan "+data[1].supplier
            mains.nomer_op.text = data[0].po_number+" dan "+data[1].po_number
            mains.nomer_pkbb.text = data[0].no_pkbb+" dan "+data[1].no_pkbb
            mains.txtTonase.text = data[0].tonase_sekarang+" dan "+data[1].tonase_sekarang
            mains.txtUmur.text = data[0].umur+" dan "+data[1].umur

        }else{
            mains.kavling.text = data[0].kavling
            mains.bahan_baku.text = data[0].bahan_baku
            mains.vendor.text = data[0].supplier
            mains.nomer_op.text = data[0].po_number
            mains.nomer_pkbb.text = data[0].no_pkbb
            mains.txtTonase.text = data[0].tonase_sekarang
            mains.txtUmur.text = data[0].umur
        }

        runningTimer()
        mains.txtStatus.visibility = GONE
    }

    fun getDataPkbb(kavling_pkbb: List<DataPkbb>) {
        dataPkbb.clear()
        dataPkbb.addAll(kavling_pkbb)
        adapter = RvAdapter(dataPkbb as ArrayList<Any?>, object : ListCallback{
            override fun onExecuteViewHolder(view: View, position: Int) {
                var txtPkbb = view.findViewById<TextView>(R.id.txtPkbb)
                var txtKavling = view.findViewById<TextView>(R.id.txtKavling)
                txtPkbb.text = dataPkbb[position].no_pkbb
                txtKavling.text = dataPkbb[position].kavling
            }

        }, mains, R.layout.rview_data_pkbb)
        Helper(mains, mains.rvDataPkbb, adapter).setRecycleview()

        if(kavling_pkbb.size > 0){
            mains.txtStatusPkbb.text = "Sudah PKBB"
            mains.txtStatusPkbb.setTextColor(Color.parseColor("#ffffff"))
            mains.cardStatus.setBackgroundColor(Color.parseColor(Warna().green()))
            mains.cardViewCenter.setBackgroundColor(Color.parseColor(Warna().green()))
            mains.cardTitle.setBackgroundColor(Color.parseColor(Warna().green()))
            mains.cardUmur.setBackgroundColor(Color.parseColor(Warna().green()))
        }else{
            mains.txtStatusPkbb.text = "Belum PKBB"
            mains.txtStatusPkbb.setTextColor(Color.parseColor("#ffffff"))
            mains.cardStatus.setBackgroundColor(Color.parseColor("#FF8C00"))
            mains.cardViewCenter.setBackgroundColor(Color.parseColor("#FF8C00"))
            mains.cardTitle.setBackgroundColor(Color.parseColor("#FF8C00"))
            mains.cardUmur.setBackgroundColor(Color.parseColor("#FF8C00"))
            mains.txtStatus.visibility = VISIBLE
        }
    }

    fun showFormLogin() {
        mains.finish()
        mains.startActivity<FormLoginActivity>()
    }

    private fun runningTimer() {
        timer = object : CountDownTimer(8000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                try {
                    Log.e("Masuk Timer", "OK")
                } catch (ex: Exception) {
                    this.cancel()
                    Log.e("Error", ex.message.toString())
                }
            }

            override fun onFinish() {
                var kavling = Intent(main.getActivity(), KavlingActivity::class.java)
                kavling.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                main.getActivity().startActivity(kavling)
            }
        }
        timer.start()
    }
}