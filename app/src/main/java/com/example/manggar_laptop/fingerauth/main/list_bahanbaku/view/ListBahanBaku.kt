package com.example.manggar_laptop.fingerauth.main.list_bahanbaku.view

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.manggar_laptop.fingerauth.R
import com.example.manggar_laptop.fingerauth.main.list_bahanbaku.presenter.ListBahanBakuPresenter
import kotlinx.android.synthetic.main.activity_list_bahan_baku.*

class ListBahanBaku : AppCompatActivity() {

    lateinit var presenter: ListBahanBakuPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_bahan_baku)

        presenter = ListBahanBakuPresenter(this)

        cardViewSbm.setOnClickListener {
            presenter.gotoPage("RM_SBM")
        }

        cardViewddgs.setOnClickListener {
            presenter.gotoPage("RM_DDGS")
        }

        cardViewCgm.setOnClickListener {
            presenter.gotoPage("RM_CGM")
        }

        cardViewFether.setOnClickListener {
            presenter.gotoPage("RM_FTM")
        }
    }
}
