package com.example.manggar_laptop.fingerauth.main

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.manggar_laptop.fingerauth.R
import kotlinx.android.synthetic.main.barcode.*

class BarcodeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.barcode)
        setInteData()
    }

    private fun setInteData() {
        if(intent.extras != null){
            txtBarcode.text  = intent.getStringExtra("qrcode")
        }
    }


}
