package com.example.manggar_laptop.fingerauth.utils;

import android.content.Context;
import android.content.Intent;
import com.example.manggar_laptop.fingerauth.main.MainActivity;

public class Warna {
  public String icon_action_color(){
    return "#B6B6B6";
  }

  public String icon_color(){
    return "#f7adb1";
  }

  public String white(){
    return "#ffffff";
  }

  public String red_like_orange(){
    return "#D32F2F";
  }

  public String success(){
    return "#009688";
  }

  public String error(){
    return "#e74135";
  }

  public String green(){
    return "#008577";
  }
}
