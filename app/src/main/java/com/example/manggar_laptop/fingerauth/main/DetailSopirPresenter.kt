package com.example.manggar_laptop.fingerauth.main

import android.app.Activity
import android.support.v7.widget.LinearLayoutManager
import com.android.volley.AuthFailureError
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.example.manggar_laptop.fingerauth.MainInterface
import com.example.manggar_laptop.fingerauth.modelsdb.DataSopir
import com.example.manggar_laptop.fingerauth.utils.ApiUrl
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.Volley
import com.example.manggar_laptop.fingerauth.adapter.ListDetailSopir
import com.example.manggar_laptop.fingerauth.modelsdb.DataSopirResponse
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_detail_sopir.*
import org.jetbrains.anko.design.snackbar

class DetailSopirPresenter(val main: MainInterface) {
    var detailSopir: Activity
    var layout: LinearLayoutManager
    lateinit var adapter:ListDetailSopir
    var keyEncrypt:String
    init {
        this.detailSopir = main.getActivity()
        this.layout = LinearLayoutManager(this.detailSopir)
        this.keyEncrypt = this.detailSopir.intent.getStringExtra("key")
    }

    fun getData() {
        main.showDialog()
        GlobalScope.launch(Dispatchers.Main) {
            val stringrequest = object : StringRequest(
                Request.Method.POST,
                ApiUrl.API.baseUrl(DataSopir.MODULE.FINGER_MOBILE, "getDataSopir"),
                object : Response.Listener<String> {
                    override fun onResponse(response: String) {
                        val data = Gson().fromJson(response, DataSopirResponse::class.java)
                        setListData(data.data)
                        main.hideDialog()
                    }
                },
                object : Response.ErrorListener {
                    override fun onErrorResponse(error: VolleyError) {
                        detailSopir.contentDetailSopir.snackbar("Jaringan Bermasalah")
                        main.hideDialog()
                    }
                }) {
                @Throws(AuthFailureError::class)
                override fun getParams(): Map<String, String> {
                    val params = HashMap<String, String>()
                    return params
                }
            }

            Volley.newRequestQueue(detailSopir).add(stringrequest)
        }
    }

    private fun setListData(data: List<DataSopir>) {
        adapter = ListDetailSopir(data, detailSopir, keyEncrypt)
        detailSopir.rvDataDetailSopir.adapter = adapter
        detailSopir.rvDataDetailSopir.layoutManager = layout
        adapter.notifyDataSetChanged()
    }
}