package com.example.manggar_laptop.fingerauth.main

import com.google.gson.annotations.SerializedName

data class KavlingModel(
    @SerializedName("kavling")
    var kavling: String
)

data class KavlingModelResponse(val data :List<KavlingModel>)
