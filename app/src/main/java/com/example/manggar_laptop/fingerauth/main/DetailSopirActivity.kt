package com.example.manggar_laptop.fingerauth.main

import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.support.v7.app.AppCompatActivity
import android.view.View.GONE
import android.view.View.VISIBLE
import com.example.manggar_laptop.fingerauth.MainInterface
import com.example.manggar_laptop.fingerauth.R
import kotlinx.android.synthetic.main.activity_detail_sopir.*
import org.jetbrains.anko.toast

class DetailSopirActivity : AppCompatActivity(), MainInterface {
    lateinit var presenter:DetailSopirPresenter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_sopir)
        presenter = DetailSopirPresenter(this)
        presenter.getData()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        this.finish()
    }

    override fun showMessage(message: String) {
        toast(message)
    }

    override fun showDialog() {
        loading.visibility = VISIBLE
    }

    override fun hideDialog() {
        loading.visibility = GONE
    }

    override fun getActivity(): FragmentActivity {
        return this
    }

    override fun reloadActivity() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
