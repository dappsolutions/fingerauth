package com.example.manggar_laptop.fingerauth

import android.content.DialogInterface

interface DialogCallback{
    fun okAction(dialog: DialogInterface, i: Int)
}