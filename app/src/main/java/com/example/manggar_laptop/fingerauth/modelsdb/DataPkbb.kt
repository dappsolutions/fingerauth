package com.example.manggar_laptop.fingerauth.modelsdb

import com.google.gson.annotations.SerializedName

data class DataPkbb(
    @SerializedName("kavling")
    var kavling:String,
    @SerializedName("pkbb")
    var no_pkbb:String
){
    object MODULE{
        const val FINGER_MOBILE = "finger_mobile"
    }
}