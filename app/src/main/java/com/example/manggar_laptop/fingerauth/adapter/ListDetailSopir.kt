package com.example.manggar_laptop.fingerauth.adapter

import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.manggar_laptop.fingerauth.R
import com.example.manggar_laptop.fingerauth.modelsdb.DataSopir
import kotlinx.android.synthetic.main.list_detail_sopir.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.jetbrains.anko.alert
import org.jetbrains.anko.okButton
import org.jetbrains.anko.toast

class ListDetailSopir(
    val list: List<DataSopir>,
    val detailSopir: Activity,
    val keyEncrypt: String
) : RecyclerView.Adapter<DetailSopirVH>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DetailSopirVH {
        val view: View
        view = LayoutInflater.from(parent.context).inflate(R.layout.list_detail_sopir, parent, false)
        return DetailSopirVH(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: DetailSopirVH, position: Int) {
        holder.bindItem(list.get(position), detailSopir, keyEncrypt)
    }

}

class DetailSopirVH(val view: View) : RecyclerView.ViewHolder(view) {
    fun bindItem(
        data: DataSopir,
        detailSopir: Activity,
        keyEncrypt: String
    ) {
        itemView.txtIdSopir.text = data.id
        itemView.cardSopir.setOnClickListener {
            var sopir = data.id
            itemView.context.alert {
                title = "Pesan"
                message = "Sidik Jari Belum Terdaftar"
                ctx.setTheme(R.style.MyDialogTheme)
            }
//            GlobalScope.launch(Dispatchers.Main) {
//                val stringRequest =
//                    object : StringRequest(
//                        Request.Method.POST,
//                        "http://10.100.2.70/timbangan.rfid.uhf/finger_mobile/insertFingerSopir",
//                        object : Response.Listener<String> {
//                            override fun onResponse(response: String) {
////                                ctx.toast("Sidik Jari Valid")
//                                it.context.alert {
//                                    title = "Kavling"
//                                    message = response
//                                    it.context.setTheme(R.style.MyDialogTheme)
//                                    okButton {
//                                        detailSopir.finish()
//                                    }
//                                }.show()
//                            }
//                        },
//                        object : Response.ErrorListener {
//                            override fun onErrorResponse(error: VolleyError) {
//                                it.context.toast("Jaringan Bermasalah")
//                            }
//                        }) {
//                        override fun getParams(): Map<String, String> {
//                            val params = HashMap<String, String>()
//                            params["user"] = sopir
//                            params["key"] = keyEncrypt
//                            return params
//                        }
//                    }
//
//                Volley.newRequestQueue(it.context).add(stringRequest)
//            }
        }
    }
}