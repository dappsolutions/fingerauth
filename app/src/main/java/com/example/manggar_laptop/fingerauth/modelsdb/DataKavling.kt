package com.example.manggar_laptop.fingerauth.modelsdb

import com.google.gson.annotations.SerializedName

data class DataKavling(
    @SerializedName("label")
    var kavling:String,
    @SerializedName("purchase_order_number")
    var po_number:String,
    @SerializedName("raw_material")
    var bahan_baku:String,
    @SerializedName("tonase_sekarang")
    var tonase_sekarang:String,
    @SerializedName("tonase_op")
    var tonase_op:String,
    @SerializedName("tonase_aktual")
    var tonase_aktual:String,
    @SerializedName("sisa_tonase")
    var sisa_tonase:String,
    @SerializedName("supplier")
    var supplier:String,
    @SerializedName("tanggal_op")
    var tanggal_op:String,
    @SerializedName("tanggal_pengiriman")
    var tanggal_pengiriman:String,
    @SerializedName("jatuh_tempo")
    var jatuh_tempo:String,
    @SerializedName("no_pkbb")
    var no_pkbb:String,
    @SerializedName("umur")
    var umur:String
){
    object MODULE{
        const val FINGER_MOBILE = "finger_mobile"
    }
}