package com.example.manggar_laptop.fingerauth.main

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.manggar_laptop.fingerauth.R
import kotlinx.android.synthetic.main.form_login.*
import org.jetbrains.anko.toast

class FormLoginActivity : AppCompatActivity() {

    lateinit var presenter: LoginPresenter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.form_login)
        presenter = LoginPresenter(this)


        btnLogin.setOnClickListener {
            presenter.execLogin()
        }
    }

    override fun onBackPressed() {
        toast("Anda Harus Login Terlebih dahulu ")
    }
}
