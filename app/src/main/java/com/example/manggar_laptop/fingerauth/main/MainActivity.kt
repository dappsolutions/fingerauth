package com.example.manggar_laptop.fingerauth.main

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.WindowManager
import com.example.manggar_laptop.fingerauth.MainInterface
import com.example.manggar_laptop.fingerauth.R
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast

class MainActivity : AppCompatActivity(), MainInterface {

    lateinit var mainPresenter: MainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        showSystemUi()
        mainPresenter = MainPresenter(this)

//        mainPresenter.initFingerPrint()
        mainPresenter.getDataKavling()

//        btnScan.setOnClickListener {
//            startActivity<ScanActivityMainActivity>()
//        }
    }

    override fun onResume() {
        super.onResume()
        mainPresenter.initFingerPrint()
        mainPresenter.getDataKavling()
    }

    override fun onBackPressed() {
//        mainPresenter.showFormLogin()
        finish()
    }

    override fun showMessage(message: String) {
        toast(message)
    }

    override fun showDialog() {
        loading.visibility = VISIBLE
        contentMain.visibility = GONE
    }

    override fun hideDialog() {
        loading.visibility = GONE
        contentMain.visibility = VISIBLE
    }

    override fun getActivity(): FragmentActivity {
        return this
    }

    override fun reloadActivity() {

    }


//    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
//        if(keyCode == KeyEvent.KEYCODE_HOME){
//            Log.e("Button", "You Press Home")
//            return false
//        }
//        return super.onKeyDown(keyCode, event)
//    }
//
//
//    override fun onAttachedToWindow() {
////        this.window.setType(WindowManager.LayoutParams.TYPE_KEYGUARD_DIALOG)
//        Log.e("Window", "OKKK")
//        super.onAttachedToWindow()
//    }
//
//    override fun onUserLeaveHint() {
//        Log.e("HOME BUTTON", "home buttonn brooo")
////        super.onUserLeaveHint()
//    }
//
//    override fun onNewIntent(intent: Intent?) {
//        Log.e("Intent", "Baru Intent Bro")
//        super.onNewIntent(intent)
//    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        if(hasFocus) hideSystemWindows()
    }

    fun hideSystemWindows(){
        window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_IMMERSIVE
                or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_FULLSCREEN)
    }

    fun showSystemUi(){
        window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN)
    }
}
