package com.example.manggar_laptop.fingerauth

import android.view.View

interface ListCallback {
  fun onExecuteViewHolder(view:View, position:Int)
}