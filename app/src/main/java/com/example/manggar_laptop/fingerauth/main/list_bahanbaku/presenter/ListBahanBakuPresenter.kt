package com.example.manggar_laptop.fingerauth.main.list_bahanbaku.presenter

import com.example.manggar_laptop.fingerauth.main.KavlingActivity
import com.example.manggar_laptop.fingerauth.main.list_bahanbaku.view.ListBahanBaku
import org.jetbrains.anko.startActivity


class ListBahanBakuPresenter(val main: ListBahanBaku) {
    fun gotoPage(id_bahan_baku:String){
        main.startActivity<KavlingActivity>("id_bahan_baku" to id_bahan_baku)
    }
}