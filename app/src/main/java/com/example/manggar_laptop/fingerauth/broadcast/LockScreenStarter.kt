package com.example.manggar_laptop.fingerauth.broadcast

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import com.example.manggar_laptop.fingerauth.main.MainActivity

class LockScreenStarter : BroadcastReceiver(){
    override fun onReceive(context: Context?, intent: Intent?) {
        Log.e("Hasil", "Masuk ON")
        if(intent?.action.equals(Intent.ACTION_SCREEN_ON)){
            Log.e("Hasil", "Masuk")
            val main = Intent(context, MainActivity::class.java)
            main.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context?.startActivity(main)
        }
    }

}